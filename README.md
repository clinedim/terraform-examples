## Getting AWS Ready for Terraform

*Create S3 Bucket*

This is used to store Terraform state. Suggestions:

- The S3 Bucket name should match the GitLab repository name.
- Block all public access.
- Turn on versioning.

*Create a DynamoDB Table*

This is used to store Terraform's lock state.

- The DynamoDB table name should match the GitLab repository name.

*Create a Amazon ECR Repository*

This is used to store any Docker containers used during deployments.

## Configure Terraform

*Create a `deploy` Directory for Terraform Files*

*Create a `deploy/main.tf` File*

- Set up Terraform to use S3 bucket to store state.

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform-examples-clinedim"
    dynamodb_table = "terraform-examples-tf-state-lock"
    encrypt = true
    key = "terraform-examples.tfstate"
    region = "us-east-1"
  }
}
```

- Set up AWS as the provider.

```hcl-terraform
provider "aws" {
  region = "us-east-1"
  version = "~> 2.54.0"
}
```

## Set up Docker Compose for running Terraform

```yaml
version: "3.7"

services:
  terraform:
    environment:
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    image: hashicorp/terraform:0.12.21
    volumes:
      - .:/infra
    working_dir: /infra
```

## Initialize Terraform

*Log into AWS Vault*

- `aws-vault exec aws.iam.username.goes.here --duration=12h`

*Run the Intiailize Command*

- `docker-compose -f deploy/docker-compose.yml run --rm terraform init`

## Create a Bastion EC2 Instance

*Bastion Server:* A server that you can use to connect to your private network, inside AWS, for administrative
purposes.

- Create a `deploy/bastion.tf` file:

```hcl-terraform
data "aws_ami" "amazon_linux" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  most_recent = true
  owners      = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
}
```

## Create Terraform Workspaces

*Workspace:* A way for you to manage different environments within the same AWS account. (For instance, development,
staging, and production.)

- Show all workspaces:

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list`

- Create a workspace:

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace new dev`

## Create Resource Prefix and Tags

```hcl-terraform
variable "contact" {
  default = "marc.clinedinst@gmail.com"
}

variable "prefix" {
  default = "terraform-examples"
}

variable "project" {
  default = "terraform-examples"
}
```

```hcl-terraform
locals {
  common_tags = {
    Environment = terraform.workspace
    ManagedBy   = "Terraform"
    Owner       = var.contact
    Project     = var.project
  }
  prefix = "${var.prefix}-${terraform.workspace}"
}
```

```hcl-terraform
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}
```