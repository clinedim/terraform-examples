data "aws_ami" "amazon_linux" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  most_recent = true
  owners      = ["amazon"]
}

resource "aws_key_pair" "my_key" {
  key_name   = "my_key"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

resource "aws_instance" "example" {
  ami = data.aws_ami.amazon_linux.id
  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    private_key = file(var.PATH_TO_PRIVATE_KEY)
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
  }
  instance_type = "t2.micro"
  key_name      = aws_key_pair.my_key.key_name
  provisioner "file" {
    destination = "/tmp/script.sh"
    source      = "script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }
  security_groups = [
    aws_security_group.example.name
  ]
  tags = {
    Name = "terraform-examples"
  }
}

resource "aws_security_group" "example" {
  description = "Control inbound and outbound access"
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8000
    protocol    = "tcp"
    to_port     = 8000
  }
  name = "terraform-examples"
}