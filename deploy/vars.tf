variable "AMIS" {
  default = {
    eu-west-1 = "ami-0d729a60"
    us-east-1 = "ami-13be557e"
    us-west-1 = "ami-06b94666"
  }
}

variable "AWS_REGION" {
  default = "us-east-1"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "my_key"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "my_key.pub"
}

variable "INSTANCE_USERNAME" {
  default = "ec2-user"
}