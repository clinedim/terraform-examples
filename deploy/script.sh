#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

yum update -y
amazon-linux-extras install nginx1 -y

service nginx start