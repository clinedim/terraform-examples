terraform {
  backend "s3" {
    bucket         = "terraform-examples-clinedim"
    dynamodb_table = "terraform-examples-tf-state-lock"
    encrypt        = true
    key            = "terraform-examples.tfstate"
    region         = "us-east-1"
  }
}
